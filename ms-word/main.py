# @author Felix Huhn, felix.huhn@rwth-aachen.de
# last changes 24.09.2021

from pywinauto.application import Application
from pywinauto import findwindows

from time import sleep
import argparse
import os



def create_new_wordfile(app):
    win = app.top_window()
    win.Pane.Backstageview.HomePane.New.NewListBox.ListItem0.invoke()
    

def get_page_editor(win, page=1):
    editor = win.child_window(title="Page 1", auto_id=f"UIA_AutomationId_Word_Page_{page}", control_type="Custom").Edit
    if not editor.exists:
        raise NameError('Page does not exist.')
    return editor





def save_file_as(win, path):
    # selects the file save as dialog from menu
    # saves the currently opened file to the specified path location (without error checking)

    # click on "File" tab
    win.child_window(title="File Tab", auto_id="FileTabButton", control_type="Button").invoke()
    win.child_window(title="Save As", control_type="ListItem").select()
    win.child_window(title="Browse", control_type="Button").invoke()

    # file select dialog
    dlg = win.Dialog
    
    # pinned folders
    pinned = dlg.child_window(title="Schnellzugriff", control_type="TreeItem").sub_elements()
    for folder in pinned:
        print(folder)

    ## folders in current view
    # folders = dlg.child_window(title="Shellordneransicht", auto_id="listview", control_type="Pane").ListBox.get_items()
    ## get folder name of 0. element
    # folders[0].children()[1].get_value()

    # Edit path in toolbar
    splitted_path = os.path.split(path)
    dlg.child_window(auto_id="41477", control_type="Pane").Progress.Pane.Toolbar.buttons()[0].invoke()
    dlg.child_window(title="Adresse", auto_id="41477", control_type="Edit").type_keys(splitted_path[0] + " {ENTER}")

    # type file name
    dlg.child_window(title="File name:", auto_id="1001", control_type="Edit").type_keys(splitted_path[1])


    # hit save button
    dlg.child_window(title="Save", auto_id="1", control_type="Button").invoke()

    # replace file if it already exists    
    rpl_dlg = dlg.child_window(title="Microsoft Word", control_type="Window")
    if rpl_dlg.exists():
        print(f"dialog? {rpl_dlg.is_dialog()}")
        print(f"file {splitted_path[1]} already exists at {splitted_path[0]} Replace it!")
        rpl_dlg.child_window(title="OK", control_type="Button").invoke()


if __name__ == '__main__':
    # parsing cmd line args
    parser = argparse.ArgumentParser(description='Simple Word Example.')
    parser.add_argument('file', metavar='f', help='Location where a new .docx file should be saved. If it exists, it will be overwritten.')
    args = parser.parse_args()
    file_path = os.path.abspath(args.file)
    
    # start notepad app
    app = Application(backend='uia').start("WINWORD.exe", 2) # WINWORD has to be in path variable
    sleep(1)
    
    try:
        create_new_wordfile(app)
        sleep(1)
        win = app.top_window()
    
        p1 = get_page_editor(win)
        p1.type_keys("Hello world!", with_spaces = True)
                
        sleep(1.5)

        save_file_as(win, file_path)
        
        sleep(1.5)
        win.close()

    finally:
        # kill app in case of an error or at the end of execution
        app.kill(True)
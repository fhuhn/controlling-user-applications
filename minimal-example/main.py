# @author Felix Huhn, felix.huhn@rwth-aachen.de
# last changes 24.09.2021

from pywinauto.application import Application
from pywinauto import findwindows

from time import sleep
import argparse
import os


def control_notepad(app, file):
    win = app.top_window()
    print(win.friendly_class_name())
    # menu items
    #app_menu = win.descendants(control_type="MenuBar")[1]
    app_menu = win.menu()

    menu_items = app_menu.items()
    print("Menu items:", end=' ')
    for mi in menu_items:
        print(mi.text(), end=' ')
    print('')

    if(os.path.isfile(file)):
        dialog_open_file(app, app_menu, file)
    else:
        # save as
        dialog_save_file_as(app, app_menu, file)
    
    # reset the win variable in case a new window was created (on file open)
    win = app.top_window()
    win.wait('ready')

    # # write text
    print('writing to text file')
    win.Edit.type_keys("^{END} {ENTER}") # jump to the end of the file
    win.Edit.type_keys("The minimal example works!", with_spaces = True)

    # # wait for user to see what is happening
    sleep(1)
    print('enough, save and get out.') 
    save_file(app, app_menu)

    # friendly close
    win.close()

    # # without saving dialog 
    # close_dialog = app.EditorDialog
    # close_dialog.Button2.click()


def dialog_open_file(app, menu, path):
    # selects the file open dialog from the menu 
    # opens the file at the specified path

    # get menuitem 'File -> Open' by number
    it = menu.get_menu_path("#0 -> #2")
    it[-1].select()

    # open a new file dialog, identified by class name
    dlg = app.window(class_name='#32770')
    dlg.wait('ready')
    # type out path 
    dlg.FileNameEdit.type_keys(path)
    sleep(0.5)
    # open file
    dlg['Öffnen'].click()



def dialog_save_file_as(app, menu, path):
    # selects the file save as dialog from menu
    # saves the currently opened file to the specified path location (without error checking)

    # get menuitem 'File -> Save As' by number
    it = menu.get_menu_path("#0 -> #4")
    it[-1].select()
    # open a new file dialog, identified by class name
    dlg = app.window(class_name='#32770')
    dlg.wait('ready')
    # type out path
    dlg.FileNameEdit.type_keys(path)
    sleep(0.5)
    # save file
    dlg['Speichern'].click()



def save_file(app, menu):
    # simply selects save from menu
    # only works if the currently opened file is already saved

    # get menuitem 'File -> Save' by number
    it = menu.get_menu_path("#0 -> #3")
    it[-1].select()



if __name__ == '__main__':
    # parsing cmd line args
    parser = argparse.ArgumentParser(description='Simple Notepad Example.')
    parser.add_argument('file', metavar='f', help='File to which this examples appends a line. If it does not exist, it will be created.')
    args = parser.parse_args()
    file_path = os.path.abspath(args.file)
    
    # start notepad app
    app = Application(backend='win32').start("notepad.exe")
    try:
        control_notepad(app, file_path)
    finally:
        # kill app in case of an error or at the end of execution
        app.kill(True)

        if app.UntitledNotepad.exists():
            print(app.top_window().dump_tree() )
# @author Felix Huhn, felix.huhn@rwth-aachen.de
# last changes 04.10.2021

from pywinauto.application import Application
from comtypes import COMError

import win32gui
from pywinauto.controls.uiawrapper import UIAWrapper

from pywinauto import keyboard


from time import sleep
import argparse
import os


def empty_trash(app):
    win = app.ProgramManager
    desktop = win.Desktop
    
    # open trash from desktop (has to be on the desktop!)
    try:
        desktop.RecycleBin.invoke()
    except COMError:
        print('Error: Papierkorb is not on the desktop')
        return

    #app.wait_cpu_usage_lower(threshold=0.5, timeout=30, usage_interval=1.0)
    
    trash = app.window(title_re='Papierkorb')
    trash.wait('ready')
    
    # get "Papierkorbtools"
    ribbon = trash.UIRibbonDockTopPane.Ribbon.Ribbon2.TabControl
    trashTools = ribbon.TabItem4.select()

    # get lower menu band
    lowerMenuBand = trash.UIRibbonDockTopPane.Ribbon.Pane5
    
    # get empty trash button
    emptyTrash = lowerMenuBand.Button8

    try:
        emptyTrash.invoke()
    except COMError as e:
        print('Trash cannot be emptied\nAlready empty?\n COMError: ' + e.__str__() )
    
    dlg = trash.child_window(class_name="#32770")
    if dlg.exists():
        dlg.wait('ready')
        dlg.Button0.invoke()

    sleep(1)
    trash.close()

def create_folder_on_desktop(app):
    win = app.ProgramManager
    desktop = win.Desktop
    desktop.click_input(button="right")
    
    contextMenu = app.menu
    # print(contextMenu.items())

    newBtn = contextMenu.NeuMenuItem
    newBtn.select()

    # why so ever? sub menu is spawned in the program manager
    # create a new folder
    newSubMenu = win.menu
    newSubMenu.MenuItem0.select()
    sleep(0.5)

    keyboard.send_keys("testfolder{ENTER}", with_spaces = True)

    sleep(0.5)
    desktop.testfolder.click_input(button="right")

    contextMenu = app.menu
    deleteBtn = contextMenu.child_window(auto_id="30994")
    deleteBtn.select()

def systray(app):
    taskbar = app.window(class_name="Shell_TrayWnd")
    print(taskbar.dump_tree())
    taskbar.Button5.invoke()

    action_center = app.window(title_re="Action Center")
    print(action_center.dump_tree())

def getNotification(app):
    while True:
        try:
            notifcation = Application(backend='win32').connect(title_re="New Notification", class_name="Windows.UI.Core.CoreWindow")
            notf = notifcation.exists()
            print(f'Notification?: {notf}')
            #print(notifcation.windows())
            if notf:
                notifcation.dump_tree()
        except Exception:
            print("not found")
        
        sleep(0.5)


if __name__ == '__main__':
    # parsing cmd line args
    # parser = argparse.ArgumentParser(description='Simple Word Example.')
    # parser.add_argument('file', metavar='f', help='Location where a new .pptx file should be saved. If it exists, it will be overwritten.')
    # args = parser.parse_args()
    # file_path = os.path.abspath(args.file)
    
    # start notepad app
    app = Application(backend='uia').connect(path='explorer.exe')
    
    getNotification(app)
    # systray(app)
    # create_folder_on_desktop(app)
    # sleep(1)
    # empty_trash(app)

    input('wait')

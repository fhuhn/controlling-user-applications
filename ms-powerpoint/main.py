# @author Felix Huhn, felix.huhn@rwth-aachen.de
# last changes 03.10.2021

from pywinauto.application import Application
from pywinauto import findwindows

from time import sleep
import argparse
import os



def create_new_ppt_file(app):
    win = app.top_window()
    win.Pane.Backstageview.HomePane.New.NewListBox.ListItem0.invoke()
    
def get_ribbon_tabs(win):
    return win.child_window(title="Ribbon Tabs", control_type="Tab").children()

def get_lower_ribbon(win):
    # only child of the lower ribbon element is a group containing all the elements to the selected Tab
    return win.child_window(title="Lower Ribbon", control_type="Pane").children()[0]

def append_new_slide(win):
    # select Home tab
    get_ribbon_tabs(win)[0].select()
    
    lr = get_lower_ribbon(win)
    # invoke the "new slide" button in the lower ribbon tree
    lr.children()[1].children()[0].children()[0].invoke()



def get_slide(win, page=1):

    slide = win.child_window(title=f"Slide {page}", control_type="Custom")
    while not slide.exists():
        print(f'slide exists? {slide.exists()}')
        input('wait')

    if not slide.exists():
        raise NameError(f'Slide {page} does not exist.')
    return slide

def textbox_type_text(tb, text):
    tb.set_focus()
    tb.click_input()
    tb.type_keys(text, with_spaces=True)

def save_file_as(win, path):
    # selects the file save as dialog from menu
    # saves the currently opened file to the specified path location (without error checking)

    # click on "File" tab
    win.child_window(title="File Tab", auto_id="FileTabButton", control_type="Button").invoke()
    win.child_window(title="Save As", control_type="ListItem").select()
    win.child_window(title="Browse", control_type="Button").invoke()

    # file select dialog
    dlg = win.Dialog
    
    # pinned folders
    pinned = dlg.child_window(title="Schnellzugriff", control_type="TreeItem").sub_elements()
    for folder in pinned:
        print(folder)

    ## folders in current view
    # folders = dlg.child_window(title="Shellordneransicht", auto_id="listview", control_type="Pane").ListBox.get_items()
    ## get folder name of 0. element
    # folders[0].children()[1].get_value()

    # Edit path in toolbar
    splitted_path = os.path.split(path)
    dlg.child_window(auto_id="41477", control_type="Pane").Progress.Pane.Toolbar.buttons()[0].invoke()
    dlg.child_window(title="Adresse", auto_id="41477", control_type="Edit").type_keys(splitted_path[0] + " {ENTER}")

    # type file name
    dlg.child_window(title="File name:", auto_id="1001", control_type="Edit").type_keys(splitted_path[1])


    # hit save button
    dlg.child_window(title="Save", auto_id="1", control_type="Button").invoke()

    # replace file if it already exists    
    rpl_dlg = dlg.child_window(title="Microsoft Word", control_type="Window")
    if rpl_dlg.exists():
        print(f"dialog? {rpl_dlg.is_dialog()}")
        print(f"file {splitted_path[1]} already exists at {splitted_path[0]} Replace it!")
        rpl_dlg.child_window(title="OK", control_type="Button").invoke()


if __name__ == '__main__':
    # parsing cmd line args
    parser = argparse.ArgumentParser(description='Simple Word Example.')
    parser.add_argument('file', metavar='f', help='Location where a new .pptx file should be saved. If it exists, it will be overwritten.')
    args = parser.parse_args()
    file_path = os.path.abspath(args.file)
    
    # start notepad app
    app = Application(backend='uia').start("POWERPNT.exe", 2) # POWERPNT has to be in path variable
    sleep(1)
    
    try:
        create_new_ppt_file(app)
        sleep(1)
        win = app.top_window()
    
        s1 = get_slide(win)
        s1_title = s1.children()[0]
        s1_subtitle = s1.children()[1]
        textbox_type_text(s1_title, "Hello World?!")
        textbox_type_text(s1_subtitle, "A critical view on the \"Hello world!\" Program.")
                
        sleep(1.5)

        save_file_as(win, file_path)
        
        sleep(1.5)
        win.close()

    finally:
        # kill app in case of an error or at the end of execution
        app.kill(True)
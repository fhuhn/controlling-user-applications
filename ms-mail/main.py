from pywinauto.application import Application
from comtypes import COMError
from pywinauto import keyboard


from time import sleep
import argparse
import os


def get_ribbon_tabs(win):
    return win.child_window(title="Ribbon Tabs", control_type="Tab").children()

def get_lower_ribbon(win):
    # only child of the lower ribbon element is a group containing all the elements to the selected Tab
    return win.child_window(title="Lower Ribbon", control_type="Pane").children()[0]

def new_email(app, win):
    # select Home tab
    get_ribbon_tabs(win)[0].select()
    
    lr = get_lower_ribbon(win)
    # invoke the "new email" button in the lower ribbon tree
    newEmailBtn = lr.children()[0].children()[0].children()[0]
    newEmailBtn.invoke()

    # get window of new email
    newMailWindow = app.window(title_re="Untitled")
    return newMailWindow
    
def writeEmail(win, mailto, subject, text):
    toEdit = win.child_window(auto_id="4117", control_type="Edit")
    subjectEdit = win.child_window(auto_id="4101", control_type="Edit")
    txtEdit = win.child_window(title="Page 1 content", auto_id="Body", control_type="Edit")

    toEdit.type_keys(mailto, with_spaces=True)
    sleep(0.5)
    subjectEdit.type_keys(subject, with_spaces=True)
    sleep(0.7)
    txtEdit.type_keys(text, with_spaces=True)

def sendEmail(win):
    sendBtn = win.child_window(title="Send", auto_id="4256", control_type="Button")
    sendBtn.invoke()
    


def get_inbox(win):
    mails = win.child_window(title="Table View", auto_id="4704", control_type="Table").GroupBox.children()
    for mail in mails:
        txts = mail.texts()
        print(f'From: {txts[0]}, subject {txts[1]} at {txts[2]}, {mail.legacy_properties()["Value"]}')

def send_and_receive(win):
    # select send / receive tab
    get_ribbon_tabs(win)[1].select()
    
    lr = get_lower_ribbon(win)
    # invoke the "send / receive" button in the lower ribbon tree
    sendReceive = lr.children()[0].children()[0]
    print(sendReceive)
    #sendReceive.dump_tree()
    sendReceive.invoke()

def get_nav_destinations(win):
    # possible dests: Mail, Calendar, People, Tasks
    navbar = win.child_window(title="Navigation Bar", auto_id="4710", control_type="Group")
    
    return navbar.children()

def navigate(dests, whereto):
    for btn in dests:
        if whereto in btn.texts():
            btn.invoke()
            break
    



if __name__ == '__main__':
    # parsing cmd line args
    # parser = argparse.ArgumentParser(description='Simple Word Example.')
    # parser.add_argument('file', metavar='f', help='Location where a new .pptx file should be saved. If it exists, it will be overwritten.')
    # args = parser.parse_args()
    # file_path = os.path.abspath(args.file)
    
    # start notepad app
    # app = Application(backend='uia').start("OUTLOOK.exe", 2)
    app = Application(backend='uia').connect(path="OUTLOOK.exe")
    app.wait_cpu_usage_lower(threshold=0.5, timeout=30, usage_interval=1.0)
    win = app.top_window()
    

    
    nav_dests = get_nav_destinations(win)
    print(nav_dests)
    
    navigate(nav_dests, 'Calendar')
    sleep(1)
    navigate(nav_dests, 'Mail')

    input('wait')

    send_and_receive(win)
    app.wait_cpu_usage_lower(threshold=0.5, timeout=30, usage_interval=1.0)

    get_inbox(win)
    sleep(1)

    mailWindow = new_email(app, win)
    mailto, subject, text = "vm-test13@outlook.com", "Just Testing", "Lorem Ipsum etc bla bla."
    
    writeEmail(mailWindow, mailto, subject, text)
    sleep(1)
    # respecify the window. Because the window tile changes to the subject as soon as the subject field is filled. 
    mailWindow = app.window(title_re = subject)
    sendEmail(mailWindow)

    input('close outlook?')
    win.close()

